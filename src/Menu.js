import { Icon, Menu } from 'antd';
import { Link } from 'react-router-dom';
import React from 'react';

export default () => (
    <Menu
        mode="horizontal"
        overflowedIndicator={<Icon type="menu" />}
        defaultSelectedKeys={[window.location.hash || '#/']}
        theme="dark"
    >
        <Menu.Item key="#/">
            <Link to="/">
                <Icon type="home" /> Home
            </Link>
        </Menu.Item>
        <Menu.Item key="#/profile">
            <Link to="/profile">
                <Icon type="idcard" /> Me
            </Link>
        </Menu.Item>
        <Menu.Item key="#/articles">
            <Link to="/articles">
                <Icon type="read" /> Articles
            </Link>
        </Menu.Item>
        <Menu.Item key="#/bookmarks">
            <Link to="/bookmarks">
                <Icon type="book" /> Bookmarks
            </Link>
        </Menu.Item>
        <Menu.Item key="#/contact">
            <Link to="/contact">
                <Icon type="mail" /> Contact
            </Link>
        </Menu.Item>
    </Menu>
);
