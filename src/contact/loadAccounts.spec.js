import { loadAccounts } from './loadAccounts';
import data from '@solid/query-ldflex';
import { emptyList, list, value } from '../test-utils/ldflex';

jest.mock('@solid/query-ldflex', () => ({}));
describe('when loading accounts', () => {
    describe('successfully from profile without accounts', () => {
        beforeEach(() => {
            data['https://pod.example/profile/card#me'] = {
                foaf_account: emptyList(),
            };
        });

        it('the result is an empty list', async () => {
            const result = await loadAccounts(
                'https://pod.example/profile/card#me'
            );
            expect(result).toEqual([]);
        });
    });
    describe('successfully from a profile with one account', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/profile/card#me'] = {
                foaf_account: list(['https://pod.example/accounts#1']),
            };
            data['https://pod.example/accounts#1'] = {
                sioc_name: value('Some account'),
                foaf_homepage: value(
                    'https://service.example/user/some-account'
                ),
                foaf_accountServiceHomepage: {
                    label: value('Some service'),
                },
            };
            result = await loadAccounts('https://pod.example/profile/card#me');
        });

        it('the result contains one account', () => {
            expect(result).toHaveLength(1);
        });

        it('and the account has a name', () => {
            expect(result[0].name).toBe('Some account');
        });

        it('and the account has a homepage', () => {
            expect(result[0].homepage).toBe(
                'https://service.example/user/some-account'
            );
        });

        it('and the account has a service name', () => {
            expect(result[0].serviceName).toBe('Some service');
        });
    });

    describe('successfully from a profile with three accounts', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/profile/card#me'] = {
                foaf_account: list([
                    'https://pod.example/accounts#1',
                    'https://pod.example/accounts#2',
                    'https://pod.example/accounts#3',
                ]),
            };
            data['https://pod.example/accounts#1'] = {
                foaf_accountServiceHomepage: {},
            };
            data['https://pod.example/accounts#2'] = {
                foaf_accountServiceHomepage: {},
            };
            data['https://pod.example/accounts#3'] = {
                foaf_accountServiceHomepage: {},
            };
            result = await loadAccounts('https://pod.example/profile/card#me');
        });

        it('the result contains three accounts', () => {
            expect(result).toHaveLength(3);
        });
    });

    describe('successfully with minimal data', () => {
        let result;
        beforeEach(async () => {
            data['https://pod.example/profile/card#me'] = {
                foaf_account: list(['https://pod.example/accounts#1']),
            };
            data['https://pod.example/accounts#1'] = {
                foaf_accountServiceHomepage: {},
            };
            result = await loadAccounts('https://pod.example/profile/card#me');
        });

        it('the account name falls back to the uri', () => {
            expect(result[0].name).toBe('https://pod.example/accounts#1');
        });

        it('and the account has no homepage', () => {
            expect(result[0].homepage).toBeUndefined();
        });

        it('and the account has no service name', () => {
            expect(result[0].serviceName).toBeUndefined();
        });
    });
});
