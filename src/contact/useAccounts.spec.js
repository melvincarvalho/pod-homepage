import React from 'react';
import { mount } from 'enzyme';
import { loadAccounts } from './loadAccounts';
import { useAccounts } from './useAccounts';
import { act } from 'react-dom/test-utils';

jest.mock('./loadAccounts');

describe('use accounts hook', () => {
    let states;

    const TestComponent = () => {
        const state = useAccounts('http://pod.example/profile/card#me');
        states.push(state);
        return null;
    };

    describe('and successfully load accounts', () => {
        beforeAll(async () => {
            jest.resetAllMocks();
            states = [];
            loadAccounts.mockResolvedValue(['account1', 'account2']);
            await act(async () => {
                await mount(<TestComponent />);
            });
        });

        it('loads accounts', () => {
            expect(loadAccounts).toHaveBeenCalledTimes(1);
            expect(loadAccounts).toHaveBeenCalledWith(
                'http://pod.example/profile/card#me'
            );
        });

        it('renders 3 times', () => {
            expect(states).toHaveLength(3);
        });

        describe('after mounting', () => {
            it('returns an empty account list', () => {
                expect(states[1].accounts).toEqual([]);
            });

            it('indicates loading', () => {
                expect(states[1].loading).toBe(true);
            });

            it('indicates no error', () => {
                expect(states[1].error).toBeFalsy();
            });
        });

        describe('after loading accounts finished', () => {
            it('returns an empty account list', () => {
                expect(states[2].accounts).toEqual(['account1', 'account2']);
            });

            it('indicates that loading finished', () => {
                expect(states[2].loading).toBe(false);
            });

            it('indicates no error', () => {
                expect(states[2].error).toBeFalsy();
            });
        });
    });

    describe('and fail to load accounts', () => {
        beforeAll(async () => {
            jest.resetAllMocks();
            states = [];
            loadAccounts.mockRejectedValue('failed');
            await act(async () => {
                await mount(<TestComponent />);
            });
        });

        it('tries to load accounts', () => {
            expect(loadAccounts).toHaveBeenCalledTimes(1);
            expect(loadAccounts).toHaveBeenCalledWith(
                'http://pod.example/profile/card#me'
            );
        });

        it('renders 3 times', () => {
            expect(states).toHaveLength(3);
        });

        describe('after mounting', () => {
            it('returns an empty account list', () => {
                expect(states[1].accounts).toEqual([]);
            });

            it('indicates loading', () => {
                expect(states[1].loading).toBe(true);
            });

            it('indicates no error', () => {
                expect(states[1].error).toBeFalsy();
            });
        });

        describe('after loading accounts failed', () => {
            it('returns an empty account list', () => {
                expect(states[2].accounts).toEqual([]);
            });

            it('indicates that loading finished', () => {
                expect(states[2].loading).toBe(false);
            });

            it('indicates no error', () => {
                expect(states[2].error).toBe('failed');
            });
        });
    });
});
