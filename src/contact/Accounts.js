import React from 'react';

import { List } from 'antd';
import { useAccounts } from './useAccounts';

export const Accounts = ({ webId }) => {
    const { accounts, loading } = useAccounts(webId);
    return (
        <List
            header="You can find me at the following services"
            grid={{
                gutter: 16,
                xs: 1,
                sm: 2,
                md: 4,
            }}
            loading={loading}
            itemLayout="horizontal"
            dataSource={accounts}
            renderItem={AccountItem}
        />
    );
};

const AccountItem = ({ homepage, name, serviceName }) => (
    <List.Item extra={<a href={homepage}>{name}</a>}>
        <List.Item.Meta title={serviceName} />
    </List.Item>
);
