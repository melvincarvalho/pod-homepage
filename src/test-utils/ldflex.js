import { toIterablePromise } from 'ldflex';

export const value = string => Promise.resolve({ toString: () => string });

export const emptyList = () => toIterablePromise(async function* empty() {});

export const list = items => {
    async function* allItems() {
        while (items.length) {
            const item = items.shift();
            yield value(item);
        }
    }
    return toIterablePromise(allItems);
};
