import data from '@solid/query-ldflex';

export const loadProfile = async webId => {
    const person = data[webId];
    const name = await person.name;
    const image = await person.image;
    const photo = await person.vcard_hasPhoto;
    const homepage = await person.homepage;

    const role = await person.vcard_role;
    const organization = await person['vcard:organization-name'];

    return {
        webId,
        name: name ? `${name}` : webId,
        imageSrc: (image && `${image}`) || (photo && `${photo}`),
        role: role && `${role}`,
        organization: organization && `${organization}`,
        homepage: homepage && `${homepage}`,
    };
};
