import { useBookmarks } from './useBookmarks';
import React from 'react';
import { List } from 'antd';

export default ({ webId }) => {
    const { bookmarks, loading } = useBookmarks(webId);
    return (
        <List
            header="My bookmarks"
            itemLayout="horizontal"
            loading={loading}
            dataSource={bookmarks}
            renderItem={item => (
                <List.Item>
                    <List.Item.Meta
                        title={item.title}
                        description={<a href={item.recalls}>{item.recalls}</a>}
                    />
                </List.Item>
            )}
        />
    );
};
