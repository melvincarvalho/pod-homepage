/**
 * @jest-environment node
 */

import loadBlogPosts from '../loadBlogPosts';
import { givenMolid } from 'molid/lib/molid-jest';

describe('load post', () => {
    givenMolid('with a blog and only one post', molid => {
        describe('when posts of the blog are loaded', () => {
            let posts;
            beforeAll(async () => {
                posts = await loadBlogPosts(molid.uri('/blog#it'));
            });

            it('an array containing one post is returned', async () => {
                expect(posts).toEqual([
                    {
                        uri: molid.uri('/post1#it'),
                        title: 'The one and only',
                        created: new Date('2019-11-22'),
                        blog: {
                            homepage: molid.uri('/blog'),
                            name: 'Test Blog',
                        },
                    },
                ]);
            });
        });
    });

    givenMolid('with a blog and three posts', molid => {
        describe('when posts of the blog are loaded', () => {
            let posts;
            beforeAll(async () => {
                posts = await loadBlogPosts(molid.uri('/blog#it'));
            });

            it('an array containing three posts is returned', async () => {
                expect(posts).toHaveLength(3);
            });

            it('contains each uri', () => {
                const uris = posts.map(it => it.uri);
                expect(uris).toContain(molid.uri('/posts#1'));
                expect(uris).toContain(molid.uri('/posts#2'));
                expect(uris).toContain(molid.uri('/posts#3'));
            });

            it('contains each title', () => {
                const titles = posts.map(it => it.title);
                expect(titles).toContain('First post');
                expect(titles).toContain('Second post');
                expect(titles).toContain('Third post');
            });

            it('contains each creation date', () => {
                const dates = posts.map(it => it.created);
                expect(dates).toContainEqual(new Date('2019-11-22'));
                expect(dates).toContainEqual(new Date('2019-11-21'));
                expect(dates).toContainEqual(new Date('2019-11-20'));
            });

            it('all posts refer to same blog', () => {
                const blogs = posts.map(it => it.blog);
                expect(blogs[0]).toEqual(blogs[1]);
                expect(blogs[1]).toEqual(blogs[2]);
            });

            it('contains blog homepage', () => {
                expect(posts[0].blog.homepage).toEqual(molid.uri('/blog'));
            });

            it('contains blog name', () => {
                expect(posts[0].blog.name).toEqual('Blog with 3 posts');
            });
        });
    });
});
