/**
 * @jest-environment node
 */

import { givenMolid } from 'molid/lib/molid-jest';
import loadPostsFromAllBlogs from '../loadPostsFromAllBlogs';

describe("load posts from user's blogs", () => {
    givenMolid('with a blog and three posts', molid => {
        describe("when the posts of all the person's blogs are loaded", () => {
            let posts;
            beforeEach(async () => {
                posts = await loadPostsFromAllBlogs(molid.uri('/person#me'));
            });

            it('an array containing three posts is returned', async () => {
                expect(posts).toHaveLength(3);
            });

            it('contains each uri', () => {
                const uris = posts.map(it => it.uri);
                expect(uris).toContain(molid.uri('/posts#1'));
                expect(uris).toContain(molid.uri('/posts#2'));
                expect(uris).toContain(molid.uri('/posts#3'));
            });

            it('contains each title', () => {
                const titles = posts.map(it => it.title);
                expect(titles).toContain('First post');
                expect(titles).toContain('Second post');
                expect(titles).toContain('Third post');
            });

            it('contains each creation date', () => {
                const dates = posts.map(it => it.created);
                expect(dates).toContainEqual(new Date('2019-11-22'));
                expect(dates).toContainEqual(new Date('2019-11-21'));
                expect(dates).toContainEqual(new Date('2019-11-20'));
            });

            it('posts are ordered by creation date (new to old)', () => {
                expect(posts[0].created.getTime()).toBeGreaterThan(
                    posts[1].created.getTime()
                );
                expect(posts[1].created.getTime()).toBeGreaterThan(
                    posts[2].created.getTime()
                );
            });

            it('all posts refer to same blog', () => {
                const blogs = posts.map(it => it.blog);
                expect(blogs[0]).toEqual(blogs[1]);
                expect(blogs[1]).toEqual(blogs[2]);
            });

            it('contains blog homepage', () => {
                expect(posts[0].blog.homepage).toEqual(molid.uri('/blog'));
            });

            it('contains blog name', () => {
                expect(posts[0].blog.name).toEqual('Blog with 3 posts');
            });
        });
    });
    givenMolid('with two blogs each containing 2 posts', molid => {
        let posts;
        beforeEach(async () => {
            posts = await loadPostsFromAllBlogs(molid.uri('/person#me'));
        });

        it('an array containing 4 posts is returned', async () => {
            expect(posts).toHaveLength(4);
        });

        it('all posts are ordered chronologically old to new', () => {
            expect(posts[0].title).toBe('Blog 2: Second post');
            expect(posts[1].title).toBe('Blog 1: Second post');
            expect(posts[2].title).toBe('Blog 2: First post');
            expect(posts[3].title).toBe('Blog 1: First post');
        });

        it.each([[1], [3]])(
            'post at position %s refers to first blog',
            postIndex => {
                expect(posts[postIndex].blog.homepage).toEqual(
                    molid.uri('/blog1')
                );
                expect(posts[postIndex].blog.name).toEqual('First blog');
            }
        );

        it.each([[0], [2]])(
            'post at position %s refers to second blog',
            postIndex => {
                expect(posts[postIndex].blog.homepage).toEqual(
                    molid.uri('/blog2')
                );
                expect(posts[postIndex].blog.name).toEqual('Second blog');
            }
        );
    });
});
