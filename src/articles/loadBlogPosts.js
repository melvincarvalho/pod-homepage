import data from '@solid/query-ldflex';
import loadPost from './loadPost';

export default async uri => {
    const blogName = await data[uri].label;
    const blogHomepage = await data[uri].foaf_homepage;
    const items = data[uri].sioc_container_of;
    const pendingPosts = [];
    for await (const post of items) {
        pendingPosts.push(loadPost(post.toString()));
    }

    const posts = await Promise.all(pendingPosts);

    return posts.map(post => ({
        ...post,
        blog: {
            name: `${blogName}`,
            homepage: `${blogHomepage}`,
        },
    }));
};
