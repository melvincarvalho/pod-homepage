import data from '@solid/query-ldflex';
import loadBlogPosts from './loadBlogPosts';

export default async webId => {
    const weblogs = data[webId].foaf_weblog;
    const promises = []
    for await (const blog of weblogs) {
        const promise = loadBlogPosts(blog.toString());
        promises.push(promise);
    }
    const result = await Promise.all(promises)
    return result.flat().sort(function(a, b) {
        return b.created - a.created;
    });
};
